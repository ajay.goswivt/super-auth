// import 'package:get_it/get_it.dart';
// import 'package:injectable/injectable.dart';
//
// import '../../DI/injection.dart';
// import 'app_config.dart';
//
// class AppEnv {
//   Future<void> injectFlavor({String? flavor}) async {
//     // final flavor =
//     //     await const MethodChannel('flavor').invokeMethod('getFlavor');
//     // debugPrint('Running Env ${flavor.toString()}');
//     // if (flavor == Environment.dev) {
//     //   await startDevelopment();
//     // } else if (flavor == Environment.test) {
//     //   await startUat();
//     // } else if (flavor == Environment.prod) {
//     //   await startProduction();
//     // } else {
//     //   await startDevelopment();
//     // }
//     await startDevelopment();
//   }
//   //
//   Future<void> startDevelopment() async {
//     await configureInjection(environment: Environment.dev);
//     GetIt.I<AppConfig>().initialize(
//       appName: 'DEV',
//       flavorName: Environment.dev,
//       baseUrl: 'https://rfid.goswivt.com/api/v1',
//       // port: 80,
//     );
//   }
//
//   Future<void> startUat() async {
//     await configureInjection(environment: Environment.test);
//     GetIt.I<AppConfig>().initialize(
//       appName: 'UAT',
//       flavorName: 'uat',
//       baseUrl: 'https://rfid.goswivt.com/api/v1',
//     );
//   }
//
//   Future<void> startProduction() async {
//     await configureInjection(environment: Environment.prod);
//     GetIt.I<AppConfig>().initialize(
//         appName: '',
//         flavorName: 'production',
//         baseUrl: 'https://rfid.goswivt.com/api/v1');
//   }
// }
