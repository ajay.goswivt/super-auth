class SuperKeys {
  static const String isAppFistTime = 'fistTime';
  static const String tokenKey = 'accessToken';
  static const String password = 'password';
  static const String userKey = 'user';
  static const String userBiometricKey = 'userBiometric';
  static const String searchHistoryName = 'searchHistoryName';
  static const String searchHistoryDate = 'searchHistoryDate';
  static const String cartCount = 'cartCount';
  static const String cartBox = 'cartBox';
  static const String wishlistBox = 'wishlistBox';
  static const APP_STORE_URL =
      'https://apps.apple.com/us/app/bhavika/id1550546054';
  static const PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=com.bhavikaonline.bhavika';
}
