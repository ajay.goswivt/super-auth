import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SvgIconButton extends StatelessWidget {
  final String svgPath;
  final VoidCallback onPressed;

  const SvgIconButton(
      {Key? key, required this.svgPath, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(onPressed: onPressed, icon: SvgPicture.asset(svgPath));
  }
}
