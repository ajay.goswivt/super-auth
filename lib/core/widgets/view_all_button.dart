import 'package:flutter/material.dart';

class ViewAllButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;
  const ViewAllButton({Key? key, this.title = 'View All', this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: onPressed,
        child: Text(
          title,
          style: Theme.of(context)
              .textTheme
              .button!
              .copyWith(color: Theme.of(context).primaryColor),
        ));
  }
}
