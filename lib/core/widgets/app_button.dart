import 'package:flutter/material.dart';

enum ButtonWidth { large, small }

class AppButton extends StatelessWidget {
  final String buttonText;
  final ButtonWidth buttonWidth;
  final EdgeInsets padding;
  final GestureTapCallback? onPressed;
  final Duration duration;
  final Widget? leadingWidget;
  final Color? backgroundColor;
  final Color? textColor;
  final OutlinedBorder outlinedBorder;

  const AppButton({
    Key? key,
    required this.buttonText,
    this.onPressed,
    this.buttonWidth = ButtonWidth.large,
    this.padding = const EdgeInsets.all(10),
    this.duration = const Duration(seconds: 5),
    this.leadingWidget,
    this.textColor,
    this.backgroundColor,
    this.outlinedBorder = const StadiumBorder(),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: padding,
        // color: Colors.red,
        height: MediaQuery.of(context).size.height / 16,
        width: buttonWidth == ButtonWidth.small
            ? MediaQuery.of(context).size.width / 3.5
            : MediaQuery.of(context).size.width / 1.10,
        child: ElevatedButton(
          onPressed: onPressed,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              leadingWidget ?? const SizedBox(),
              Text(
                buttonText,
                style: TextStyle(
                    color: textColor,
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2.2,
                    overflow: TextOverflow.ellipsis),
              )
            ],
          ),
          style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white, elevation: 0, backgroundColor: backgroundColor ?? Theme.of(context).colorScheme.secondary,
              shape: outlinedBorder),
        ));
  }
}
