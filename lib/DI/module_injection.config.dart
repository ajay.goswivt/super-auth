// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:flutter_rest_client/flutter_rest_client.dart' as _i12;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i9;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i6;

import '../core/config/app_config.dart' as _i3;
import '../features/auth/biometric/bloc/biometric_bloc.dart' as _i25;
import '../features/auth/data/repositories/auth_remote_repository.dart' as _i14;
import '../features/auth/domain/controller/auth_login_controller.dart' as _i20;
import '../features/auth/domain/repositories/i_auth_remote_repository.dart'
    as _i13;
import '../features/auth/presentation/bloc/forgot_password_bloc/change_forgot_password_bloc/change_forgot_password_bloc.dart'
    as _i26;
import '../features/auth/presentation/bloc/forgot_password_bloc/reset_password_bloc/reset_password_bloc.dart'
    as _i23;
import '../features/auth/presentation/bloc/forgot_password_bloc/verify_opt_bloc/verify_otp_bloc.dart'
    as _i24;
import '../features/auth/presentation/bloc/login_bloc/login_bloc.dart' as _i21;
import '../features/auth/presentation/bloc/register_bloc/register_bloc.dart'
    as _i22;
import '../features/auth/social/data/repository/i_social_repository.dart'
    as _i15;
import '../features/auth/social/data/repository/social_auth_repository.dart'
    as _i16;
import '../features/auth/social/presentation/social_bloc.dart' as _i18;
import '../features/user/data/repositories/user_remote_repostiory.dart' as _i11;
import '../features/user/data/session/app_manager.dart' as _i5;
import '../features/user/data/session/i_app_manager.dart' as _i4;
import '../features/user/data/session/i_session_manager.dart' as _i7;
import '../features/user/data/session/session_manager.dart' as _i8;
import '../features/user/domain/controller/user_controller.dart' as _i17;
import '../features/user/domain/repositories/i_user_remote_repository.dart'
    as _i10;
import '../features/user/presentation/bloc/user_bloc.dart'
    as _i19; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initModuleGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  gh.lazySingleton<_i3.AppConfig>(() => _i3.AppConfig());
  gh.factory<_i4.IAppManager>(
      () => _i5.AppManager(get<_i6.SharedPreferences>()));
  gh.factory<_i7.ISessionManager>(
      () => _i8.SessionManager(get<_i9.FlutterSecureStorage>()));
  gh.factory<_i10.IUserRemoteRepository>(() => _i11.UserRemoteRepository(
        get<_i12.IHttpHelper>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i13.IAuthRemoteRepository>(() => _i14.AuthRemoteRepository(
        get<_i12.IHttpHelper>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i15.ISocialAuthRepository>(
      () => _i16.SocialAuthRepository(get<_i13.IAuthRemoteRepository>()));
  gh.factory<_i17.IUserController>(
      () => _i17.UserController(get<_i10.IUserRemoteRepository>()));
  gh.factory<_i18.SocialBloc>(() => _i18.SocialBloc(
        get<_i15.ISocialAuthRepository>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i19.UserBloc>(() => _i19.UserBloc(
        get<_i17.IUserController>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i20.IAuthLoginController>(() => _i20.AuthLoginController(
        get<_i13.IAuthRemoteRepository>(),
        get<_i7.ISessionManager>(),
        get<_i4.IAppManager>(),
      ));
  gh.factory<_i21.LoginBloc>(() => _i21.LoginBloc(
        get<_i20.IAuthLoginController>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i22.RegisterBloc>(() => _i22.RegisterBloc(
        get<_i20.IAuthLoginController>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i23.ResetPasswordBloc>(
      () => _i23.ResetPasswordBloc(get<_i20.IAuthLoginController>()));
  gh.factory<_i24.VerifyOtpBloc>(() => _i24.VerifyOtpBloc(
        get<_i20.IAuthLoginController>(),
        get<_i7.ISessionManager>(),
      ));
  gh.factory<_i25.BiometricBloc>(() => _i25.BiometricBloc(
        get<_i13.IAuthRemoteRepository>(),
        get<_i7.ISessionManager>(),
        get<_i20.IAuthLoginController>(),
        get<_i4.IAppManager>(),
      ));
  gh.factory<_i26.ChangeForgotPasswordBloc>(() => _i26.ChangeForgotPasswordBloc(
        get<_i20.IAuthLoginController>(),
        get<_i7.ISessionManager>(),
      ));
  return get;
}
