import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../auth/data/models/user_model.dart';
import '../../data/session/i_session_manager.dart';
import '../../domain/controller/user_controller.dart';

part 'user_event.dart';
part 'user_state.dart';

@Injectable()
class UserBloc extends Bloc<UserEvent, UserState> {
  final ISessionManager sessionManager;
  final IUserController controller;

  UserBloc(this.controller, this.sessionManager) : super(UserInitial()){
    on<UserProfileFetchEvent>((event, emit) async{
        emit(UserProfileLoading());
        final localUser = await sessionManager.getCurrentUser();
        if (localUser != null) {
          emit(UserProfileLoadSuccess(user: localUser));
        }
        final response = await controller.getUserProfile();
        if (response.ok && response.response != null) {
          emit(UserProfileLoadSuccess(user: response.response!));
          return;
        }
        emit(UserProfileLoadFailure(
            message: response.message ?? 'An Unknown Error Occurred.'));
      });

    on<UserProfileUpdateEvent>((event, emit) async{
      emit (UserInformationUpdating());
      final result = await controller.userInformationUpdate(
          model: event.userModel, token: event.token);
      if (result.ok) {
        emit( UserInformationUpdateSuccess(phone: result.data?.phone ?? ''));
        return;
      }
      emit(UserInformationUpdateFailure(
          message: result.message ?? 'An unknown error occurred.'));
    });


  }
}
