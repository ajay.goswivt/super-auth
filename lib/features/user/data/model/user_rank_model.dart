import 'package:json_annotation/json_annotation.dart';

import '../../../auth/data/models/user_model.dart';

part 'user_rank_model.g.dart';

@JsonSerializable()
class UserRankModel {
  final int rank;
  final UserModel user;

  UserRankModel(this.rank, this.user);

  factory UserRankModel.fromJson(Map<String, dynamic> json) =>
      _$UserRankModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserRankModelToJson(this);
}
