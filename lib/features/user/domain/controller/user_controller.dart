import 'package:flutter_rest_client/flutter_rest_client.dart';
import 'package:injectable/injectable.dart';

import '../../../auth/data/models/user_model.dart';
import '../../data/model/user_response_model.dart';
import '../repositories/i_user_remote_repository.dart';


abstract class IUserController {
  Future<ResponseEntity<UserModel>> getUserProfile();

  Future<UserResponseModel> userInformationUpdate(
      {required UserModel model, String? token});
}

@Injectable(as: IUserController)
class UserController implements IUserController {
  final IUserRemoteRepository remoteRepository;

  UserController(this.remoteRepository);

  @override
  Future<UserResponseModel> userInformationUpdate(
      {required UserModel model, String? token}) {
    return remoteRepository.userInformationUpdate(model: model, token: token);
  }

  @override
  Future<ResponseEntity<UserModel>> getUserProfile() {
    return remoteRepository.getUserProfile();
  }
}
