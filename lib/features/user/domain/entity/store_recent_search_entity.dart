class StoreRecentSearchEntity {
  final String name;
  final String searchDate;

  StoreRecentSearchEntity({required this.name, required this.searchDate});
}
