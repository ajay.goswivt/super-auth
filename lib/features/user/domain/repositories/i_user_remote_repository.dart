import 'package:flutter_rest_client/flutter_rest_client.dart';

import '../../../auth/data/models/user_model.dart';
import '../../data/model/user_response_model.dart';


abstract class IUserRemoteRepository {
  Future<ResponseEntity<UserModel>> getUserProfile();

  Future<UserResponseModel> userInformationUpdate(
      {required UserModel model, required String? token});
}
