import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../../../user/data/session/i_session_manager.dart';
import '../../../../domain/controller/auth_login_controller.dart';


part 'change_forgot_password_event.dart';
part 'change_forgot_password_state.dart';

@injectable
class ChangeForgotPasswordBloc
    extends Bloc<ChangeForgotPasswordEvent, ChangeForgotPasswordState> {
  IAuthLoginController controller;
  ISessionManager sessionManager;

  ChangeForgotPasswordBloc(this.controller, this.sessionManager)
      : super(ChangeForgotPasswordInitial()){

    on<ChangeForgotPasswordButtonTapEvent>((event, emit) async {
      emit (ChangeForgotPasswordLoading());
      final data = await controller.changeForgotPassword(
          oldPassword: event.oldPassword,
          newPassword: event.newPassword,
          token: event.token);
      if (data.ok) {
        await sessionManager.clearSession();
        emit (ChangeForgotPasswordLoadSuccess(
            message: data.message ?? 'Password Changed Successfully.'));
      } else {
        emit (ChangeForgotPasswordLoadFailure(
            errorMessage: data.message ?? 'An unknown error occurred.'));
      }
    });
  }
  //
  // @override
  // Stream<ChangeForgotPasswordState> mapEventToState(
  //   ChangeForgotPasswordEvent event,
  // ) async* {
  //   if (event is ChangeForgotPasswordButtonTapEvent) {
  //     yield ChangeForgotPasswordLoading();
  //     final data = await controller.changeForgotPassword(
  //         oldPassword: event.oldPassword,
  //         newPassword: event.newPassword,
  //         token: event.token);
  //     if (data.ok) {
  //       await sessionManager.clearSession();
  //       yield ChangeForgotPasswordLoadSuccess(
  //           message: data.message ?? 'Password Changed Successfully.');
  //     } else {
  //       yield ChangeForgotPasswordLoadFailure(
  //           errorMessage: data.message ?? 'An unknown error occurred.');
  //     }
  //   }
  // }
}
