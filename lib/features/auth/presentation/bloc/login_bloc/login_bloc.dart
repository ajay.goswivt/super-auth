import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../../user/data/session/i_session_manager.dart';
import '../../../data/models/login_model.dart';
import '../../../domain/controller/auth_login_controller.dart';


part 'login_event.dart';
part 'login_state.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final IAuthLoginController controller;
  final ISessionManager sessionManager;

  LoginBloc(this.controller, this.sessionManager) : super(LoginInitialState()){
    on<LoginButtonTapEvent>((event, emit) async {
      emit (LoginLoadingState());
      try {
        final response = await controller.authLogin(
            email: event.email, password: event.password,phone: event.phone, role: event.role);
        if (response.ok) {
          final localUser = await sessionManager.getCurrentUser();
          if (response.user?.id != localUser?.id) {
            await sessionManager.clearBiometrics();
          }
          emit (LoginSuccessState(response));
          return;
        }
        emit(LoginFailureState(errorMessage: response.message!));
      } catch (e) {
        emit (LoginFailureState(errorMessage: e.toString()));
    }
    });
  }

}
