import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../user/data/session/i_session_manager.dart';
import '../../data/models/social/social_model.dart';
import '../data/repository/i_social_repository.dart';


part 'social_event.dart';
part 'social_state.dart';

enum SocialType { Facebook, Google, Apple }

@injectable
class SocialBloc extends Bloc<SocialEvent, SocialState> {
  late SocialModel socialModel;
  final ISocialAuthRepository socialAuthRepository;
  final ISessionManager iSessionManager;

  SocialBloc(this.socialAuthRepository, this.iSessionManager)
      : super(SocialInitial()){

    on<SocialLoginTapEvent>((event, emit)  async{
      emit (SocialStateLoading());
      switch (event.socialType) {
        case SocialType.Facebook:
          socialModel = await socialAuthRepository.facebookLogin();
          break;
        case SocialType.Google:
          socialModel = await socialAuthRepository.googleLoginIn();
          break;
        case SocialType.Apple:
          socialModel = await socialAuthRepository.appleLogin();
          break;
      }
      if (socialModel.ok) {
        await iSessionManager.saveToken(accessToken: socialModel.accessToken!);
        if (socialModel.user != null) {
          await iSessionManager.saveCurrentUser(user: socialModel.user!);
        }
        emit (SocialStateSuccess(socialModel: socialModel));
        return;
      }
      emit (SocialStateFailed(
          message: socialModel.message ?? 'An unknown error occured.'));
    });
  }

  // @override
  // Stream<SocialState> mapEventToState(
  //   SocialEvent event,
  // ) async* {
  //   if (event is SocialLoginTapEvent) {
  //     yield SocialStateLoading();
  //     switch (event.socialType) {
  //       case SocialType.Facebook:
  //         socialModel = await socialAuthRepository.facebookLogin();
  //         break;
  //       case SocialType.Google:
  //         socialModel = await socialAuthRepository.googleLoginIn();
  //         break;
  //       case SocialType.Apple:
  //         socialModel = await socialAuthRepository.appleLogin();
  //         break;
  //     }
  //     if (socialModel.ok) {
  //       await iSessionManager.saveToken(accessToken: socialModel.accessToken!);
  //       if (socialModel.user != null) {
  //         await iSessionManager.saveCurrentUser(user: socialModel.user!);
  //       }
  //       yield SocialStateSuccess(socialModel: socialModel);
  //       return;
  //     }
  //     yield SocialStateFailed(
  //         message: socialModel.message ?? 'An unknown error occured.');
  //   }
  // }
}
